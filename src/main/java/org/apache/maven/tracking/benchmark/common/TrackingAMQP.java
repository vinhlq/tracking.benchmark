package org.apache.maven.tracking.benchmark.common;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP.Queue;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.Exchange;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.client.Method;

public class TrackingAMQP {
	public TrackingAMQP(ConnectionFactory factory, Connection connection, String exchangeName, String routingKey, String type) throws IOException, TimeoutException {
		this.factory = factory;
		this.connection=connection;
		this.connection.addShutdownListener(new ShutdownListener() {

			@Override
			public void shutdownCompleted(ShutdownSignalException cause) {
				if (cause.isHardError()) {
					Connection conn = (Connection) cause.getReference();
					if (!cause.isInitiatedByApplication()) {
						Method reason = cause.getReason();
						throw new RuntimeException("[AMQP] harderror " + reason.toString());
					}
				} else {
					Channel ch = (Channel) cause.getReference();
				}

			}

		});
	    
	    this.channel = this.connection.createChannel();
	    this.workingExchange=exchangeName;
	    
	    this.channel.exchangeDeclare(this.workingExchange, type);
	    this.defaultQueue = this.channel.queueDeclare();
	    this.channel.queueBind(this.defaultQueue.getQueue(), this.workingExchange, routingKey);
	    this.binding = 	this.factory.getHost() + ":" + this.factory.getPort() + ":" +
	    				this.workingExchange + ":" + type + ":" + this.defaultQueue.getQueue() + ":" + routingKey;
	}
	
	public TrackingAMQP(ConnectionFactory factory, Connection connection, String exchangeName, String routingKey) throws IOException, TimeoutException{
		this(factory, connection, exchangeName, routingKey, "topic");
	}
	
	public TrackingAMQP(ConnectionFactory factory, String exchangeName, String routingKey, String type, int poolCount) throws IOException, TimeoutException{
		this(factory, DefaultConnection(factory, poolCount), exchangeName, routingKey, type);
	}
	
	public TrackingAMQP(String host, int port, String exchangeName, String routingKey, String type, int poolCount) throws IOException, TimeoutException{
	    this(DefaultConnectionFactory(host, port), exchangeName, routingKey, type, poolCount);
	}

	
	
	public static Connection DefaultConnection(ConnectionFactory factory, int poolCount) throws IOException, TimeoutException {
		if(poolCount < 1)
			poolCount = 1;
		ExecutorService es = Executors.newFixedThreadPool(poolCount);
		return factory.newConnection(es);
	}
	
	public static ConnectionFactory DefaultConnectionFactory(String host, int port) {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		factory.setPort(port);
		factory.setRequestedHeartbeat(60);
		factory.setConnectionTimeout(5000);
		factory.setAutomaticRecoveryEnabled(true);
		factory.setTopologyRecoveryEnabled(true);
		factory.setNetworkRecoveryInterval(1000);
		
		return factory;
	}
	
	/* start waiting for message on default queue */
	public void basicConsume(Consumer consumer) throws IOException{
		this.channel.basicConsume(this.defaultQueue.getQueue(), false, consumer);
	}
	public void basicConsume(Consumer consumer, boolean autoAck) throws IOException{
		this.channel.basicConsume(this.defaultQueue.getQueue(), autoAck, consumer);
	}
	
	/* publish on working channel */
	public void basicPublish(String routingKey, boolean mandatory, boolean immediate,  BasicProperties props, byte[] body) throws IOException{
		this.channel.basicPublish(this.workingExchange, routingKey, mandatory, immediate, props, body);
	}
	
	public Channel getChannel(){
		return this.channel;
	}
	
	public String getBinding(){
		return this.binding;
	}
	
	private Queue.DeclareOk defaultQueue;
	private ConnectionFactory factory;
	private Connection connection;
	private String workingExchange;
	private Channel channel;
	private String binding;
}
