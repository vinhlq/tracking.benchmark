package org.apache.maven.tracking.benchmark.util;

public class PPS {
	class Packet{
		Packet(){
			this.count=0;
			this.last=System.currentTimeMillis();
		}
		long count;
		long last;
	}
	
	public PPS(int interval){
		this.peak=0;
		this.avg=0;
		this.pps=0;
		this.last=0;
		this.count=0;
		this.interval=interval;

		this.total=new Packet();
		this.average=new Packet();
		this.current=new Packet();
	}
	public PPS(){
		this(100);
	}
	
	public synchronized void Update(int n){
		this.average.count+=n;
		this.current.count+=n;
		this.total.count+=n;
	}
	public synchronized float Calc(){
		float ppscurrent=0;
		long currentInterval=0;
		/* current */
		currentInterval=System.currentTimeMillis()-this.current.last;
		if(currentInterval > this.interval){
			ppscurrent = this.last = (float) (this.current.count * 1000 / currentInterval);
		}
		else{
			ppscurrent = this.last;
		}
		this.current.last=System.currentTimeMillis();
		this.current.count=0;
		if(this.peak < ppscurrent){
			this.peak = ppscurrent;
		}
		
		/* from beginning */
		currentInterval=System.currentTimeMillis()-this.total.last;
		if(currentInterval > this.interval){
			this.pps=(float) (this.total.count * 1000 / currentInterval);
		}
		this.count=this.total.count;
		
		/* average */
		if(ppscurrent==0){
			this.average=new Packet();
		}
		currentInterval=System.currentTimeMillis()-this.average.last;
		if(currentInterval > this.interval){
			this.avg=(float) (this.average.count*1000/currentInterval);
		}
		return ppscurrent;
	}
	private Packet average;
	private Packet current;
	private Packet total;
	private int interval;
	public long count;
	public float peak;
	public float pps;
	public float avg;
	public float last;
}
