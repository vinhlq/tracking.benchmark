package org.apache.maven.tracking.benchmark.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class NetConsole implements Runnable {
	private Thread thread;
	private ServerSocket listener;
	private int interval = 4000;
	private int port = 19189;

	public NetConsole() {
		this.interval = 4000;
		this.port = 19189;
	}

	public NetConsole(int port, int interval) {
		this.port = port;
		if (interval > 0)
			this.interval = interval;
		else
			this.interval = 4000;
	}

	abstract public void out(OutputStream s) throws RuntimeException;

	@Override
	public void run() throws RuntimeException {
		try {
			this.listener = new ServerSocket(this.port);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + " started");
		while (true) {
			try {
				Socket socket = listener.accept();
				while (true) {
					try {
						if (!socket.isConnected())
							break;
						this.out(socket.getOutputStream());
						Thread.sleep(this.interval);
					} catch (RuntimeException e) {
						/* catch stream error */
						socket.close();
						break;
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void start() {
		if (this.thread == null) {
			this.thread = new Thread(this, "net console");
			this.thread.start();
		}
	}
}
