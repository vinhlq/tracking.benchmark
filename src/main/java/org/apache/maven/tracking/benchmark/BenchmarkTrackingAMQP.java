package org.apache.maven.tracking.benchmark;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import org.apache.maven.tracking.benchmark.util.*;
import org.apache.maven.tracking.proto.TrackingProto;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.apache.maven.tracking.benchmark.common.*;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;

import com.sun.management.OperatingSystemMXBean;

/* GPSLOG + GPSReport */
class LoggingWorker extends TrackingAMQP{
	PPS workerpps;
	Consumer consumer;
	
	public LoggingWorker(String host, int port, String exchange, String routingKey, int poolCount, PPS pps)
			throws IOException, TimeoutException {
		super(host, port, exchange, routingKey, "topic", poolCount);
		// TODO Auto-generated constructor stub
		this.workerpps=pps;
	}
	
	// share connection factory
	public LoggingWorker(ConnectionFactory factory, String exchange, String routingKey, int poolCount, PPS pps) throws IOException, TimeoutException{
		super(factory, exchange, routingKey, "topic", poolCount);
		this.workerpps=pps;
	}
	
	// share connection
	public LoggingWorker(ConnectionFactory factory, Connection connection, String exchange, String routingKey, PPS pps) throws IOException, TimeoutException{
		super(factory, connection, exchange, routingKey, "topic");
		this.workerpps=pps;
	}

	public void start() throws IOException{
		this.consumer = new DefaultConsumer(this.getChannel()) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				TrackingProto.Data.Builder proto=TrackingProto.Data.newBuilder();
//				String message = new String(body, "UTF-8");
//				System.out.println(" [x] Received '");
				try{
					proto.mergeFrom(body);
//					System.err.println(trackingProto.getLoc(0));
				}
				catch(Exception e){
					System.err.println( "[WTF] " + e.getClass().getName() + ": " + e.getMessage() );
				}
//				this.getChannel().basicAck(envelope.getDeliveryTag(), true);
				LoggingWorker.this.workerpps.Update(1);
			}
		};
		
		this.basicConsume(this.consumer, true);
		System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + "(" + this.getBinding() + ")" + " started");
	}
}

// stdout pps monitor
class PPSReportStdout extends TimerTask {
	private PPS pps;
	private Timer timer;
	private long start;
	private int logincount=0;
	private long locCount;
	private long drvCount;
	private long taxiCount;

	public long getLocCount() {
		return locCount;
	}

	public void setLocCount(long locCount) {
		this.locCount = locCount;
	}

	public long getDrvCount() {
		return drvCount;
	}

	public void setDrvCount(long drvCount) {
		this.drvCount = drvCount;
	}

	public long getTaxiCount() {
		return taxiCount;
	}

	public void setTaxiCount(long taxiCount) {
		this.taxiCount = taxiCount;
	}
	
	public synchronized void setLoginCount(int n){
		this.logincount=n;
	}
	
	synchronized int getLoginCount(){
		return this.logincount;
	}
	
	public PPSReportStdout(Timer timer, PPS pps) {
		this.timer = timer;
		this.start = System.currentTimeMillis();
		this.logincount=0;
		this.locCount=0;
		this.drvCount=0;
		this.taxiCount=0;
		this.pps=pps;
	}

	@Override
	public void run() {
//		System.out.println("Cancelling timer");
//		timer.cancel();
		float ppscurrent=this.pps.Calc();
		System.out.print("\u001b[2J\u001b[0;0H");
		System.out.printf("[%d] Runtime: %d(min)\r\n", this.getLoginCount(), (System.currentTimeMillis()-this.start)/1000/60);
		
		OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		System.out.printf("CPU: %.1f%%\r\n", operatingSystemMXBean.getProcessCpuLoad() * 100);
		
		double heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
		List<MemoryPoolMXBean> memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
		double total=0;
		for (MemoryPoolMXBean m:memoryPoolMXBeans ){
			total += m.getUsage().getUsed();
		}
		System.out.printf("RAM: %.1f:%.1f(MB)\r\n", heap/1024/1024, total/1024/1024);
		
		System.out.printf("Location: %10d\r\n" +
						  "Taxi:     %10d\r\n" +
						  "DRV:      %10d\r\n",
						  this.getLocCount(), this.getTaxiCount(), this.getDrvCount());
		System.out.printf("Tracking: [%10d] current: %8.1f, avg: %8.1f, average: %8.1f, peak: %8.1f(pps)\r\n",
							this.pps.count, ppscurrent, this.pps.avg, this.pps.pps, this.pps.peak);
	}
}

// telnet pps monitor
class NetReport extends NetConsole{
	private PPS pps;
	private long start;
	private int logincount=0;
	
	public NetReport(int port, int interval, PPS pps) {
		this.pps=pps;
		this.start = System.currentTimeMillis();
	}
	
	public synchronized void setLoginCount(int n){
		this.logincount=n;
	}
	
	synchronized int getLoginCount(){
		return this.logincount;
	}

	@Override
	public void out(OutputStream s) throws RuntimeException {
		// TODO Auto-generated method stub
		PrintWriter out = new PrintWriter(s, true);
		float ppscurrent=this.pps.Calc();
		out.print("\u001b[2J\u001b[0;0H");
		out.printf("[%d] Runtime: %d(min)\r\n", this.getLoginCount(), (System.currentTimeMillis()-this.start)/1000/60);
		out.printf("Tracking: [%10d] current: %8.1f, avg: %8.1f, average: %8.1f, peak: %8.1f(pps)\r\n",
							this.pps.count, ppscurrent, this.pps.avg, this.pps.pps, this.pps.peak);
		
		if(out.checkError())
		{
		    throw new RuntimeException("Error transmitting data.");
		}
	}
	
}

public class BenchmarkTrackingAMQP {
	static TrackingAMQP tracking;
	static TrackingProto.Data.Builder trackingProto;
	static Timer timer;
	static PPS pps;
	static Properties properties = new Properties();
	static PPSReportStdout report;
	static NetReport netreport;
	
	static class LoginRequest{
		public String getImei() {
			return imei;
		}
		public void setImei(String imei) {
			this.imei = imei;
		}
		public int getHwv() {
			return hwv;
		}
		public void setHwv(int hwv) {
			this.hwv = hwv;
		}
		public int getSwv() {
			return swv;
		}
		public void setSwv(int swv) {
			this.swv = swv;
		}
		public String getCcid() {
			return ccid;
		}
		public void setCcid(String ccid) {
			this.ccid = ccid;
		}
		private String imei;
		private int hwv;
		private int swv;
		private String ccid;
	}
	
	static class LoginResponse{
		public String getImei() {
			return imei;
		}
		public void setImei(String imei) {
			this.imei = imei;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		String imei;
		int id;
	}
	
	static class Location{
		private long time;
		private int did;
		private float lat;
		private float lon;
		private int speed;
		public long getTime() {
			return time;
		}
		public void setTime(long time) {
			this.time = time;
		}
		public int getDid() {
			return did;
		}
		public void setDid(int did) {
			this.did = did;
		}
		public float getLat() {
			return lat;
		}
		public void setLat(float lat) {
			this.lat = lat;
		}
		public float getLon() {
			return lon;
		}
		public void setLon(float lon) {
			this.lon = lon;
		}
		public int getSpeed() {
			return speed;
		}
		public void setSpeed(int speed) {
			this.speed = speed;
		}
	}
	
	static class Taxi{
		private int id;
		private String message;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}
	
	static class Driver{
		private int id;	/* device id */
		private long time;
		private int index; /* driver index */
		private String name;
		private String license;
		private String uid;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public long getTime() {
			return time;
		}
		public void setTime(long time) {
			this.time = time;
		}
		public int getIndex() {
			return index;
		}
		public void setIndex(int did) {
			this.index = did;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getLicense() {
			return license;
		}
		public void setLicense(String license) {
			this.license = license;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		
	}
	
	static class Command{
		private String name;
		private String option;
		
		public Command(String name, String option){
			this.name = name;
			this.option=option;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getOption() {
			return option;
		}
		public void setOption(String option) {
			this.option = option;
		}
		
	}
	
	public static void main( String[] args ) throws IOException, TimeoutException
    {
		properties.load(new FileInputStream("./benchmark.conf"));
		String amqpHost=properties.getProperty("AMQP_HOST").replaceAll("[\"']","");
		int amqpPort=Integer.parseInt(properties.getProperty("AMQP_PORT"));
		
		/*
		 * monitor module
		 */
		pps=new PPS();
		timer = new Timer();
		report=new PPSReportStdout(timer, pps);
		timer.schedule(report, 0, 4000);
		
		netreport=new NetReport(19189, 4000, pps);
		netreport.start();
		
		
		/*
		 *  tracking module example
		 */
		// share connection
		ConnectionFactory factory = TrackingAMQP.DefaultConnectionFactory(amqpHost, amqpPort);
		Connection connection = TrackingAMQP.DefaultConnection(factory, 40);
		for(int i=0;i<10;i++){
			LoggingWorker lw=new LoggingWorker(factory, connection, "tracking.data", "data.p"+Integer.toString(i), pps);
			lw.start();
		}
		
//		for(int i=0;i<10;i++){
//		LoggingWorker lw=new LoggingWorker(properties.getProperty("AMQP_HOST").replaceAll("[\"']",""),
//								Integer.parseInt(properties.getProperty("AMQP_PORT")),
//								"tracking.data", "data.p"+Integer.toString(i), 20, pps);
//		lw.start();
//	}

		/*
		 *  login module example
		 */
		class LoginWorker extends TrackingAMQP{
			Consumer consumer;
			private Hashtable<String,Integer> hashtable;
			
			public LoginWorker(ConnectionFactory factory, Connection connection) throws IOException, TimeoutException {
				super(factory, connection, "tracking.login", "login.request", "direct");
				// TODO Auto-generated constructor stub
				this.hashtable= new Hashtable<String,Integer>();
			}
			
			public void start() throws IOException{
				this.consumer = new DefaultConsumer(this.getChannel()) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
							byte[] body) throws IOException
					{
						try{
							String json=new String(body, "US-ASCII");
							// parse login request
							ObjectMapper mapper = new ObjectMapper();
							LoginRequest resquest = mapper.readValue(json, LoginRequest.class);
							
							// get response
							LoginResponse response = LoginWorker.this.AutoLoginHandle(resquest);
							mapper = new ObjectMapper();
							BasicProperties props = new BasicProperties.Builder().build();
							// send response
							LoginWorker.this.basicPublish(properties.getReplyTo(), false, false, props, mapper.writeValueAsString(response).getBytes());
						}
						catch(Exception e){
							System.err.println( "[WTF] " + e.getStackTrace() );
						}
						report.setLocCount(report.getLocCount()+1);
					}
				};
				this.basicConsume(this.consumer, false);
				System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + "(" + this.getBinding() + ")" + " started");
			}
			
			private LoginResponse AutoLoginHandle(LoginRequest req) {
				LoginResponse res = new LoginResponse();
				int id=1;
				
				try{
					id=this.hashtable.get(req.getImei());
				}
				catch(Exception e){
					if(this.hashtable.size() > 0){
						id=(int)hashtable.size();
					}
					this.hashtable.put(req.getImei(), id);
				}
				res.setImei(req.getImei());
				res.setId(id);
				/* report login count */
				netreport.setLoginCount(this.hashtable.size());
				report.setLoginCount(this.hashtable.size());
				return res;
			}
		}
		
//		LoginWorker autologin = new LoginWorker(amqpHost, amqpPort, 10, new AutoLoginHandle());
		LoginWorker autologin = new LoginWorker(factory, connection);
		autologin.start();
		
		/*
		 *  location module example
		 */
		class LocationWorker extends TrackingAMQP{
			Consumer consumer;
			
			public LocationWorker(ConnectionFactory factory, Connection connection) throws IOException, TimeoutException {
				super(factory, connection, "tracking.event", "location.*", "topic");
				// TODO Auto-generated constructor stub
			}

			public void start() throws IOException{
				this.consumer = new DefaultConsumer(this.getChannel()) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
							byte[] body) throws IOException
					{
						try{
							String json=new String(body, "US-ASCII");
							// parse location
							ObjectMapper mapper = new ObjectMapper();
							Location loc = mapper.readValue(json, Location.class);
							/* process loc data */
						}
						catch(Exception e){
							System.err.println( "[WTF] " + e.getStackTrace() );
						}
						report.setLocCount(report.getLocCount()+1);
					}
				};
				this.basicConsume(this.consumer, true);
				System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + "(" + this.getBinding() + ")" + " started");
			}
		}
		LocationWorker locCount = new LocationWorker(factory, connection);
		locCount.start();
		
		/*
		 *  Taxi module example
		 */
		class TaxiWorker extends TrackingAMQP{
			Consumer consumer;
			
			public TaxiWorker(ConnectionFactory factory, Connection connection) throws IOException, TimeoutException {
				super(factory, connection, "tracking.event", "taxi.up.*", "topic");
				// TODO Auto-generated constructor stub
			}

			public void start() throws IOException{
				this.consumer = new DefaultConsumer(this.getChannel()) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
							byte[] body) throws IOException
					{
						try{
//							String json=new String(body, "US-ASCII");
//							// parse location
//							ObjectMapper mapper = new ObjectMapper();
//							Taxi tx = mapper.readValue(json, Taxi.class);
							/* process taxi data */
							
							int id=Integer.parseInt(envelope.getRoutingKey().split("\\.")[2]);
							if((id % 10) == 0){
								BasicProperties props = new BasicProperties.Builder().build();
								// send response
								String routingKey="taxi.down."+Integer.toString(id);
								TaxiWorker.this.basicPublish(routingKey, false, false, props, "RES:TXI=1,99\r\n".getBytes());
							}
						}
						catch(Exception e){
							System.err.println( "[WTF] " + e.getStackTrace() );
						}
						report.setTaxiCount(report.getTaxiCount()+1);
					}
				};
				this.basicConsume(this.consumer, true);
				System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + "(" + this.getBinding() + ")" + " started");
			}
		}
		TaxiWorker txCount = new TaxiWorker(factory, connection);
		txCount.start();
		
		/*
		 *  Driver module example
		 */
		class DriverWorker extends TrackingAMQP{
			Consumer consumer;
			
			public DriverWorker(ConnectionFactory factory, Connection connection) throws IOException, TimeoutException {
				super(factory, connection, "tracking.event", "driver.up.*", "topic");
				// TODO Auto-generated constructor stub
			}

			public void start() throws IOException{
				this.consumer = new DefaultConsumer(this.getChannel()) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
							byte[] body) throws IOException
					{
						try{
							String json=new String(body, "US-ASCII");
							// parse location
							ObjectMapper mapper = new ObjectMapper();
							Driver drv = mapper.readValue(json, Driver.class);
							/* process drv data */
							
							int id=Integer.parseInt(envelope.getRoutingKey().split("\\.")[2]);
							if((id % 16) == 0){
								Command cmd=new Command("DRV", "\"10\",\"No register\",\"800073110000\",\"1a2b3c4da1b2c3d4\"");
								mapper = new ObjectMapper();
								BasicProperties props = new BasicProperties.Builder().build();
								// send response
								String routingKey="command."+Integer.toString(id);
								DriverWorker.this.basicPublish(routingKey, false, false, props, mapper.writeValueAsString(cmd).getBytes());
							}
						}
						catch(Exception e){
							System.err.println( "[WTF] " + e.getStackTrace() );
						}
						report.setDrvCount(report.getDrvCount()+1);
					}
				};
				this.basicConsume(this.consumer, true);
				System.out.println("[" + Integer.toHexString(System.identityHashCode(this)) + "]" + this.getClass().getName() + "(" + this.getBinding() + ")" + " started");
			}
		}
		DriverWorker drvCount = new DriverWorker(factory, connection);
		drvCount.start();
	}
}
