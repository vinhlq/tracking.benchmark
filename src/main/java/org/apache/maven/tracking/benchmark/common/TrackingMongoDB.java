package org.apache.maven.tracking.benchmark.common;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import java.util.Date;

public class TrackingMongoDB {
	public TrackingMongoDB(){
		this._objectid=new ObjectId();
	}
	public TrackingMongoDB(ObjectId objectid){
		this._objectid=objectid;
	}
	public TrackingMongoDB(Date date, int machineIdentifier, short processIdentifier, int counter) {
		this._objectid=new ObjectId(date,machineIdentifier,processIdentifier,counter);
	}
	public int getReceivedTime() {
		return receivedTime;
	}
	public void setReceivedTime(int receivedTime) {
		this.receivedTime = receivedTime;
	}
	public int getPacketIndex() {
		return packetIndex;
	}
	public void setPacketIndex(int packetIndex) {
		this.packetIndex = packetIndex;
	}
	public int getdIndex() {
		return dIndex;
	}
	public void setdIndex(int dIndex) {
		this.dIndex = dIndex;
	}
	public int getAdc1() {
		return adc1;
	}
	public void setAdc1(int adc1) {
		this.adc1 = adc1;
	}
	public int getAdc2() {
		return adc2;
	}
	public void setAdc2(int adc2) {
		this.adc2 = adc2;
	}
	public int getAdc3() {
		return adc3;
	}
	public void setAdc3(int adc3) {
		this.adc3 = adc3;
	}
	public int getIo_satus() {
		return io_satus;
	}
	public void setIo_satus(int io_satus) {
		this.io_satus = io_satus;
	}
	public int getLon() {
		return lon;
	}
	public void setLon(int lon) {
		this.lon = lon;
	}
	public int getLat() {
		return lat;
	}
	public void setLat(int lat) {
		this.lat = lat;
	}
	public int[] getSpeedGPS() {
		return speedGPS;
	}
	public void setSpeedGPS(int[] speedGPS) {
		this.speedGPS = speedGPS;
	}
	public int getSpeedECU() {
		return speedECU;
	}
	public void setSpeedECU(int speedECU) {
		this.speedECU = speedECU;
	}
	public int[] getLatDiff() {
		return latDiff;
	}
	public void setLatDiff(int[] latDiff) {
		this.latDiff = latDiff;
	}
	public int[] getLonDiff() {
		return lonDiff;
	}
	public void setLonDiff(int[] lonDiff) {
		this.lonDiff = lonDiff;
	}
	public int getFuel() {
		return fuel;
	}
	public void setFuel(int fuel) {
		this.fuel = fuel;
	}
	public int getSignalGSM() {
		return signalGSM;
	}
	public void setSignalGSM(int signalGSM) {
		this.signalGSM = signalGSM;
	}
	public int getSignalGPS() {
		return signalGPS;
	}
	public void setSignalGPS(int signalGPS) {
		this.signalGPS = signalGPS;
	}
	public int getBatt1Level() {
		return batt1Level;
	}
	public void setBatt1Level(int batt1Level) {
		this.batt1Level = batt1Level;
	}
	public int getBatt2Level() {
		return batt2Level;
	}
	public void setBatt2Level(int batt2Level) {
		this.batt2Level = batt2Level;
	}
	public int getRuntimeKM() {
		return runtimeKM;
	}
	public void setRuntimeKM(int runtimeKM) {
		this.runtimeKM = runtimeKM;
	}
	public int getRuntimeContinous() {
		return runtimeContinous;
	}
	public void setRuntimeContinous(int runtimeContinous) {
		this.runtimeContinous = runtimeContinous;
	}
	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	public int getTaxiStatus() {
		return taxiStatus;
	}
	public void setTaxiStatus(int taxiStatus) {
		this.taxiStatus = taxiStatus;
	}
	public int getTaxiMeterFare() {
		return taxiMeterFare;
	}
	public void setTaxiMeterFare(int taxiMeterFare) {
		this.taxiMeterFare = taxiMeterFare;
	}
	public int getTaxiShiftFare() {
		return taxiShiftFare;
	}
	public void setTaxiShiftFare(int taxiShiftFare) {
		this.taxiShiftFare = taxiShiftFare;
	}
	public int getTaxiShiftDistanceEmpty() {
		return taxiShiftDistanceEmpty;
	}
	public void setTaxiShiftDistanceEmpty(int taxiShiftDistanceEmpty) {
		this.taxiShiftDistanceEmpty = taxiShiftDistanceEmpty;
	}
	public int getTaxiShiftDistance() {
		return taxiShiftDistance;
	}
	public void setTaxiShiftDistance(int taxiShiftDistance) {
		this.taxiShiftDistance = taxiShiftDistance;
	}
	public int getTaxiTripCount() {
		return taxiTripCount;
	}
	public void setTaxiTripCount(int taxiTripCount) {
		this.taxiTripCount = taxiTripCount;
	}
	public int getTaxiTripFare() {
		return taxiTripFare;
	}
	public void setTaxiTripFare(int taxiTripFare) {
		this.taxiTripFare = taxiTripFare;
	}
	public int getTaxiTripDistance() {
		return taxiTripDistance;
	}
	public void setTaxiTripDistance(int taxiTripDistance) {
		this.taxiTripDistance = taxiTripDistance;
	}
	public Document toDocument(Date time) throws IllegalArgumentException, IllegalAccessException{
		Document doc = new Document();
		doc.append("_id", this._objectid);
		for (Field f : this.getClass().getDeclaredFields()) {
			if(Modifier.isPublic(f.getModifiers())){
				if(int[].class.isAssignableFrom(f.getType())){
					System.out.println("array "+f.getName() + ":" + f.get(this));
					if (byte.class.isAssignableFrom(f.getClass().getComponentType())) {
						doc.append(f.getName(), f.get(this));
						
					}
				}
				else if(int.class.isAssignableFrom(f.getType())){
					doc.append(f.getName(), f.get(this));
					System.out.println(f.getName() + ":" + f.get(this));
				}
				else{
					System.out.println("unconverted "+f.getClass().getName());
				}
			}
		}
		return doc;
	}
	public int receivedTime=0;
	private ObjectId _objectid;
	public int packetIndex=0;
	public int dIndex=0;
	public int adc1=0;
	public int adc2=0;
	public int adc3=0;
	public int io_satus=0;
	public int lon=0;
	public int lat=0;
	public int[] speedGPS={1,2,3,4,5,6,7,8,9,10};
	public int speedECU=0;
	public int[] latDiff={1,2,3,4,5,6,7,8,9};
	public int[] lonDiff={1,2,3,4,5,6,7,8,9};
	public int fuel=0;
	public int signalGSM=0;
	public int signalGPS=0;
	public int batt1Level=0;
	public int batt2Level=0;
	public int runtimeKM=0;
	public int runtimeContinous=0;
	public int runtime=0;
	public int taxiStatus=0;
	public int taxiMeterFare=0;
	public int taxiShiftFare=0;
	public int taxiShiftDistanceEmpty=0;
	public int taxiShiftDistance=0;
	public int taxiTripCount=0;
	public int taxiTripFare=0;
	public int taxiTripDistance=0;
}
