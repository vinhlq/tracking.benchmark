package org.apache.maven.tracking.benchmark;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import com.mongodb.MongoCredential;

import org.bson.Document;

import java.util.Arrays;
import com.mongodb.Block;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * Hello world!
 *
 */
public class App 
{
	
	
	public static void execmd(String cmd){
		try{
    		Runtime rt = Runtime.getRuntime();
    		Process proc = rt.exec(cmd);
    		InputStream stdin = proc.getInputStream();
    		InputStreamReader isr = new InputStreamReader(stdin);
    		BufferedReader br = new BufferedReader(isr);

    		String line = null;
    		System.out.println("<OUTPUT>");

    		while ( (line = br.readLine()) != null)
    		     System.out.println(line);
    		proc.waitFor();
    		int exitVal = proc.exitValue();
    		System.out.println("Process exitValue: " + exitVal);
    	}
    	catch(Exception e){

    	}
	}
	public static void main( String[] args )
    {		
//		try{
//			// For XML
//			ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig2.xml");
//			// For Annotation
////			ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
//			System.out.println(ctx);
//			MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
//			System.out.println(mongoOperation.getCollectionNames());
//		}catch(Exception e){
//			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//		}
		
    	try{
//    		Tracking tk=new Tracking();
//    		tk.toDocument(new Date( ));
//    		System.out.println(Integer.MAX_VALUE);
            // To connect to mongodb server
    		MongoClientURI uri = new MongoClientURI("mongodb://vinh:vinh@localhost/?authSource=tracking");
    		MongoClient mongoClient = new MongoClient(uri);
   			
            // Now connect to your databases
            MongoDatabase db = mongoClient.getDatabase( "tracking" );
            for (String name : db.listCollectionNames()) {
                System.out.println("[COLLECTION]" + name);
            }
            MongoCollection<Document> coll = db.getCollection("tracking");
            System.out.println("Connect to database successfully");
   			
         }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
    }
}
